HelperBot

## Description
This Project contains Assets utilized in my HelperBot Console Applications, originally ported from my Java Project in 2017-2018

## License
This project is Licenced as ARR or all Rights reserved during it's duration of usage, however partial usage for snippets is allowed.

Copyright (C) 2019 - 2020 CDAGaming

This Project is Licensed as All Rights Reserved, between the dates this copyright is active.\
Partial Usage of this Project is applicable towards the usage of partial snippets or educational purposes.

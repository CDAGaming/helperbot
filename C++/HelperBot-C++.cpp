#include <functional>
#include <map>
#include <cmath>
#include <fstream>
#include <string>
#include <vector>
#include <climits>
#include <iostream>
#include <algorithm>

using namespace std;

// CONSTANT VARIABLES
const double PI = 3.1415927;
const int MAXIMUM_INT = 2147483647;
const string version = "1.3.5", AppTitle = "C++ Helper Bot";

// VoidFunctionMap: Mapping for Functions that are Void methods and callable in the application
map<string, function<void()>> voidFunctionMap;

// Debug Logging (Only Activates in some Methods)
bool debugLogging = false;

#pragma region Utility_Methods
// AwaitInput: Outputs a "Press any Key to Continue" Input to await input
void AwaitInput() {
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
	system("pause");
#else
	system("read");
#endif
}

// CountDigits: Get integer of the digit result (Ex: CountDigits(112) = 1 + 1 + 2 = 4)
int CountDigits(long long inputNumber, bool summarize = false)
{
	int sum = 0;
	while (inputNumber != 0) {
		sum += inputNumber % 10;
		inputNumber = inputNumber / 10;
	}

	if (summarize && sum >= 10) {
		return CountDigits(sum, summarize);
	}
	else {
		return sum;
	}
}

#pragma warning( push )
#pragma warning( disable : 26451 )
// GetDayOfWeek: Return the Day of the Week based on the defined arguments, based on Zeller's Congruence Algorithm (Ignored C26451 "Arithmetic Overflow")
int GetDayOfWeek(long long year, int month, int day)
{
  // January and February are counted as month 13 and 14 of the previous year
  if (month <= 2)
  {
    month += 12;
    year--;
  }

  // Altered Formula from Normal Calculation in order to output better
  // Result Notes:
  // 0 -> Saturday
  // 1 -> Sunday
  // 2 -> Monday
  // 3 -> Tuesday
  // 4 -> Wednesday
  // 5 -> Thursday
  // 6 -> Friday
  return (day +
          13 * (month + 1) / 5 +
          year + year / 4 - year / 100 + year / 400)
         % 7;
}
#pragma warning( pop )

// CharacterToString: Converts inputted Character to a valid string type
string CharacterToString(char x)
{
	string s(1, x);

	return s;
}

// ToLowerCase: Port of Java function to force all string characters into their lowercase form
string ToLowerCase(string input) {
	locale loc;
	string output;

	for (string::size_type i = 0; i < input.length(); ++i)
		output += tolower(input[i]);

	return output;
}

// ToUpperCase: Port of Java function to force all string characters into their uppercase form
string ToUpperCase(string input) {
	locale loc;
	string output;

	for (string::size_type i = 0; i < input.length(); ++i)
		output += toupper(input[i]);

	return output;
}

// IsNullOrEmpty: Returns if a String is Null or Empty (Ported Java Function)
bool IsNullOrEmpty(string input) {
	return (input.empty() || input == "");
}

// PrintLine: Output a Message to the Output Stream, and flush (Go to new Line) if allowed
void PrintLine(string message = "", bool flushLine = true) {
	if (flushLine) {
		cout << message << endl;
	}
	else {
		cout << message;
	}
}

// GetFileLines: Opens a File, and Saves both each line into a vector, and the total Line count from vector size
void GetFileLines(string filename, vector<string>& textAlloc) {
	ifstream fileIn(filename);

	if (fileIn.is_open()) {
		string currentLine;
		while (getline(fileIn, currentLine)) {
			textAlloc.push_back(currentLine);
		}
	}
	else {
		PrintLine("Error: Unable to Open File");
	}
	fileIn.close();
}

// SaveFileLines: Edits or Creates a New File with the text speciified in String Vector
void SaveFileLines(string filename, vector<string>& textAlloc, bool flushLines = true) {
	ofstream fileOut(filename);

	if (fileOut.is_open()) {
		for (const auto& currentLine : textAlloc) {
			if (flushLines) {
				fileOut << currentLine << endl;
			}
			else {
				fileOut << currentLine;
			}
		}
	}
	else {
		PrintLine("Error: Unable to Edit/Create File");
	}
	fileOut.close();
}

// GetPentagonal: Determine whether an Inputted Real Number or Index is a Pentagonal, and Return the Pentagonal
pair<bool, long long> GetPentagonal(long long x, bool isIndex = false)
{
	long long n = x;

	if (!isIndex) {
		// IF not an Index Number, IE a Sum or Real Number, SQRT it as assumed then use General Equation
		n = (long long)(1 + sqrt(24 * x + 1)) / 6;
	}

	if (debugLogging) {
		PrintLine("Testing @ Pentagonal Index: " + to_string(n));
	}

	auto pentagonalNumber = n * (3 * n - 1) / 2;
	return make_pair(pentagonalNumber == x, pentagonalNumber);
}

// GetNumericAnswer: Template Function for getting an Answer for a Numeric Type Variable (Only variables done with cin)
template <typename T> T GetNumericAnswer(string question, T targetVariable, bool flushLine = false) {
	PrintLine(question, flushLine);
	cin >> targetVariable;

	if (cin.fail()) {
		PrintLine("Incorrect Input, Skipping...");
	}

	cin.clear();
	cin.ignore(MAXIMUM_INT, '\n');

	return targetVariable;
}

// GetCharacterAnswer: Asks the requested question, seeking a single character from the answer inputted
char GetCharacterAnswer(string question, char targetVariable, bool flushLine = true) {
	string inputData;
	int charIndex = 0;

	PrintLine(question, flushLine);
	getline(cin, inputData);

	if (inputData.length() > 1) {
		// If more then one character entered as input
		PrintLine("Please Input a Character Index from 0 to " + to_string(inputData.length() - 1) + " from input (" + inputData + ")", flushLine);
		cin >> charIndex;

		if (cin.fail()) {
			PrintLine("Incorrect Input, defaulting to Index 0");
			charIndex = 0;
		}
	}

	cin.clear();
	cin.ignore(MAXIMUM_INT, '\n');

	targetVariable = (!IsNullOrEmpty(inputData) ? inputData[charIndex] : targetVariable);
	return targetVariable;
}

// GetTextAnswer: Asks the Requested Question, seeking a string-based answer
string GetTextAnswer(string question, string targetVariable, bool flushLine = false) {
	PrintLine(question, flushLine);
	getline(cin, targetVariable);

	cin.clear();

	return targetVariable;
}

// RequestInput: Port from Java (AskQuestion) to call a method name from a map of void functions available
void RequestInput() {
	bool foundMethod = false;
	string requestedMethod = "";

	requestedMethod = GetTextAnswer("\nPlease Input a Method Name (Type Help for help, or Exit to quit): ", requestedMethod);

	if (!IsNullOrEmpty(requestedMethod)) {
		for (const auto mapping : voidFunctionMap) {
			if (!foundMethod && ToLowerCase(requestedMethod) == ToLowerCase(mapping.first)) {
				PrintLine("Launching Method: " + mapping.first + "...\n");
				foundMethod = true;
				voidFunctionMap[mapping.first]();
			}
		}

		if (!foundMethod) {
			PrintLine("\nError: Method Name (" + requestedMethod + ") was not found! \nPlease Contact the author if this is a mistake... \n");
		}
	}
	else {
		PrintLine("Error: Please Input a Valid and Non-Empty Method Name...");
	}

	if (ToLowerCase(requestedMethod) != "exit") {
		// If not Exiting, Request a New Input
		RequestInput();
	}
}

#pragma endregion Utilities used in Methods

// Extra Bundled Methods
#pragma region PrintApple_20191017
int Apple01() {
	int a = 0;
	a = GetNumericAnswer("Enter an Integer for a -> ", a);

	return a;
}

double Apple02() {
	int a = 0, c = 0;
	double b = 0;

	a = GetNumericAnswer("Enter an Integer for a -> ", a);
	b = GetNumericAnswer("Enter a Double for b -> ", b);
	c = GetNumericAnswer("Enter an Integer for c -> ", c);

	return a + b + c;
}

double Apple03() {
	double a = 0, b = 0;

	a = GetNumericAnswer("Enter an Double for a -> ", a);
	b = GetNumericAnswer("Enter a Double for b -> ", b);

	return a * b;
}

double Apple04() {
	double a = 0, d = 0;
	int b = 0;
	float c = 0;

	a = GetNumericAnswer("Enter an Double for a -> ", a);
	b = GetNumericAnswer("Enter a Integer for b -> ", b);
	c = GetNumericAnswer("Enter a Float for c -> ", c);
	d = GetNumericAnswer("Enter an Double for d -> ", a);

	return (a + b) / (c + d);
}

string Apple05() {
	return "I am Great";
}

int Apple06() {
	char a = 'A', b = 'A';

	a = GetCharacterAnswer("Enter a Character for a -> ", a);
	b = GetCharacterAnswer("Enter a Character for b -> ", b);

	return a + b;
}

double Apple07() {
	int a = 0, b = 0, c = 0;

	a = GetNumericAnswer("Enter an Integer for a -> ", a);
	b = GetNumericAnswer("Enter a Integer for b -> ", b);
	c = GetNumericAnswer("Enter a Integer for c -> ", c);

	return (a / b) % c;
}

void PrintApple() {
	int launchMethod = 1;
	string output = "Invalid Input, Please Try again...";

	launchMethod = GetNumericAnswer("Enter Method to Launch from 1 - 7 -> ", launchMethod);

	switch (launchMethod) {
	case (1 | 01):
		output = to_string(Apple01());
		break;
	case (2 | 02):
		output = to_string(Apple02());
		break;
	case (3 | 03):
		output = to_string(Apple03());
		break;
	case (4 | 04):
		output = to_string(Apple04());
		break;
	case (5 | 05):
		output = Apple05();
		break;
	case (6 | 06):
		output = to_string(Apple06());
		break;
	case (7 | 07):
		output = to_string(Apple07());
		break;
	default:
		output = "Invalid Input, Please Try again...";
	}

	PrintLine("Output for #" + to_string(launchMethod) + " -> " + output);
}
#pragma endregion Function Testing (PrintApple) on 10/17/2019

#pragma region Assignment5_20191025
// GenerateSeries: Outputs a Series of Pre-Allocated Numbers, including the start and ending numbers
void GenerateSeries(int startNumber = 0, int endNumber = 0) {
	PrintLine(to_string(startNumber) + ", ", false);

	for (int currentNumber = 3; currentNumber <= 9996; currentNumber += 2) {
		PrintLine(to_string(currentNumber) + ", ", false);
	}

	PrintLine(to_string(endNumber));
}

// DetermineSeries: Creates a Series of Numbers using GenerateSeries, asking for a Starting and Ending Number
void DetermineSeries() {
	int startNumber = 0, endNumber = 0;

	startNumber = GetNumericAnswer("Enter Starting Number -> ", startNumber);
	endNumber = GetNumericAnswer("Enter Ending Number -> ", endNumber);

	GenerateSeries(startNumber, endNumber);
}

// GetSeriesSum: Outputs a Sum of a Linear Series based on Rules for 3 Inputted Integers
void GetSeriesSum(int c = 0, int d = 0, int e = 0) {
	int seriesSum = 0, postNumber = 0;

	// Calculate Post Number to add after each set
	for (int currentPostNumber = 999; currentPostNumber >= 9; currentPostNumber -= 9) {
		postNumber += currentPostNumber;
	}

	for (int currentSeries = 0; currentSeries <= 10; currentSeries++) {
		seriesSum += (c + (2 * currentSeries)) * (d - (10 - currentSeries)) * (e + (10 - currentSeries)) * postNumber;
	}

	PrintLine("Final Sum of the Series: " + to_string(seriesSum));
}

// DetermineSeriesSum: Retreive a Sum of a Series of Numbers using GetSeriesSum, asking for 3 Integers
void DetermineSeriesSum() {
	int c = 0, d = 0, e = 0;

	c = GetNumericAnswer("Enter First Number -> ", c);
	d = GetNumericAnswer("Enter Second Number -> ", d);
	e = GetNumericAnswer("Enter Third Number -> ", e);

	GetSeriesSum(c, d, e);
}

// GetCompatibility: Retrieves a Compatibility Number by determining 2 Life Numbers and Retreiving the Remainder
void GetCompatibility(int m1 = 0, int d1 = 0, int y1 = 0, int m2 = 0, int d2 = 0, int y2 = 0) {
	int lifeNumber01 = 0, lifeNumber02 = 0;
	int yearLifeIndex01 = 0, yearLifeIndex02 = 0;

	int compatibilityNumber = 0;

	// Ex: 1977 = (1 + 9 + 7 + 7) = 24 = (2 + 4) = 6
	yearLifeIndex01 = CountDigits(y1, true);
	yearLifeIndex02 = CountDigits(y2, true);

	lifeNumber01 = CountDigits((long long)m1 + d1 + yearLifeIndex01);
	lifeNumber02 = CountDigits((long long)m2 + d2 + yearLifeIndex02);

	PrintLine("Life Number 1: " + to_string(lifeNumber01));
	PrintLine("Life Number 2: " + to_string(lifeNumber02));

	if (lifeNumber01 > lifeNumber02) {
		compatibilityNumber = lifeNumber01 % lifeNumber02;
	}
	else {
		compatibilityNumber = lifeNumber02 % lifeNumber01;
	}

	PrintLine("Compatibility Number: " + to_string(compatibilityNumber));
}

// DetermineCompatibility: Determine you and your Partner's Compatibility Number using GetCompatibility, using your birth dates
void DetermineCompatibility() {
	int m1 = 0, d1 = 0, y1 = 0, m2 = 0, d2 = 0, y2 = 0;

	m1 = GetNumericAnswer("Input your Birth Month (Format: XX or X) -> ", m1);
	d1 = GetNumericAnswer("Input your Birth Day (Format: XX or X) -> ", d1);
	y1 = GetNumericAnswer("Input your full Birth Year (Format: XXXX) -> ", y1);

	m2 = GetNumericAnswer("Input your partner's Birth Month (Format: XX or X) -> ", m2);
	d2 = GetNumericAnswer("Input your partner's Birth Day (Format: XX or X) -> ", d2);
	y2 = GetNumericAnswer("Input your partner's full Birth Year (Format: XXXX) -> ", y2);

	GetCompatibility(m1, d1, y1, m2, d2, y2);
}
#pragma endregion Data for Assignment 5, done on 10/25/2019

#pragma region Methods

// Help: Output All Valid Methods in a Help Dialog
void Help() {
	PrintLine(AppTitle + " - Valid Methods:");

	for (auto const& element : voidFunctionMap) {
		PrintLine(" - " + element.first);
	}
}

// DigitAdder: Digits outputted as all digits added together
void DigitAdder() {
	int inputNumber = 0, sum = 0;

	inputNumber = GetNumericAnswer("Enter a Number: ", inputNumber);

	string convertedString = to_string(inputNumber), digitString;

	// size_t: unsigned integer used to avoid Error C4018 ("signed/unsigned mismatch")
	for (size_t i = 0; i < convertedString.length(); i++) {
		digitString = digitString + convertedString.at(i);

		if (i != convertedString.length() - 1) {
			digitString = digitString + ", ";
		}
	}

	PrintLine("Digits (" + to_string(convertedString.length()) + "): [" + digitString + "]");
	PrintLine("Sum of all Digits: " + to_string(CountDigits(inputNumber)));
}

// StudentData: Getting Student Info such as Name, Favorite Class, and Student ID
void StudentData() {
	string userName, className;
	int studentID = 0;

	userName = GetTextAnswer("What's your name? ", userName);
	PrintLine("Hello " + userName + ", It's very nice to meet you! :D");

	className = GetTextAnswer("What is your favorite class, " + userName + "? ", className);
	PrintLine("That's great, I like " + className + " as well!");

	studentID = GetNumericAnswer("What is your Student ID? ", studentID);
	PrintLine("Thank You, " + userName + "! Your Student ID is " + to_string(studentID) + ".");
}

// NumberExtender: Extend a Number via operations from the initial entered number
void NumberExtender() {
	float enteredNumber = 0, extendByNumber = 0, output;
	int enteredNumber_Int, extendByNumber_Int;
	string operationID;

	operationID = GetTextAnswer("Enter Operation (Empty/Invalid = Addition): ", operationID);

	enteredNumber = GetNumericAnswer("Input a Number: ", enteredNumber);
	extendByNumber = GetNumericAnswer("Input a Number to adjust by (Negative to Reduce, Or Power if entered 'Pow' or '^' as selected operation): ", extendByNumber);

	// Ignore Error C4244 "Possible Loss of Data" as it is intentional for some operations in this function
#pragma warning( push )
#pragma warning( disable : 4244)
	enteredNumber_Int = enteredNumber;
	extendByNumber_Int = extendByNumber;
#pragma warning( pop ) 

	operationID = ToLowerCase(operationID);

	output = (operationID == "-") ? (enteredNumber - extendByNumber) :
		(operationID == "*" || operationID == "x" || operationID == "multiply" || operationID == "multiplication") ? (enteredNumber * extendByNumber) :
		(operationID == "/" || operationID == "divide" || operationID == "division") ? (enteredNumber / extendByNumber) :
		(operationID == "%" || operationID == "mod" || operationID == "modulus") ? (enteredNumber_Int % extendByNumber_Int) :
		(operationID == "^" || operationID == "pow" || operationID == "power") ? (powf(enteredNumber, extendByNumber)) :
		(operationID == "sqrt") ? (sqrtf(enteredNumber)) : (enteredNumber + extendByNumber);

	// Null Check is here as the Default Condition in output is automatically Addition so it prevent having to add a special case
	if (IsNullOrEmpty(operationID)) {
		operationID = "+";
	}

	PrintLine("Operation in Use: " + operationID);
	PrintLine("Result: " + to_string(output));
}

// ShapeAP: Getting the Area and Perimeter of a Shape
void ShapeAP() {
	float area, perimeter, length = 0, width = 0;

	length = GetNumericAnswer("Input the Length of your Shape: ", length);
	width = GetNumericAnswer("Input the Width of your Shape: ", width);

	area = length * width;
	perimeter = (length * 2) + (width * 2);

	PrintLine("Shape Area: " + to_string(area));
	PrintLine("Shape Perimeter: " + to_string(perimeter));
}

// IntToCharacter: Converting an Integer to a ASCII Character
void IntToCharacter() {
	string convertedChar;
	int initialChar = 0;

	initialChar = GetNumericAnswer("Input Number for Conversion to ASCII Character: ", initialChar);

	convertedChar = (char)initialChar;

	PrintLine("Result: " + convertedChar);
}

// SuddenQuiz01: Sudden Quiz taken on 09/12/2019
void SuddenQuiz01() {
	float weight = 0, height_Feet = 0, height_Inches = 0, height, BMI;

	weight = GetNumericAnswer("Enter your weight in pounds: ", weight);
	height_Feet = GetNumericAnswer("Enter your height in feet: ", height_Feet);
	height_Inches = GetNumericAnswer("Enter your height's remaining inches: ", height_Inches);

	// Height = Total Height in Inches
	height = (12 * height_Feet) + height_Inches;

	BMI = (703 * weight) / (pow(height, 2));

	PrintLine("Your BMI is " + to_string(BMI) + ", your weight is " + to_string(weight) + ", and your height is " + to_string(height_Feet) + " feet and " + to_string(height_Inches) + " inches.");
}

// SuddenQuiz02: Sudden Quiz taken on 10/1/2019
void SuddenQuiz02() {
	double firstSum = 0, secondSum = 0;

	for (int i = 1; i != 101; i++) {
		secondSum += i;
		firstSum += pow(i, 2);
	}

	secondSum = pow(secondSum, 2);

	PrintLine("The difference between the square of the sum (" + to_string(secondSum) + ")\nand the sum of the squares of the first one hundred natural numbers (" + to_string(firstSum) + ") is " + to_string(secondSum - firstSum) + ".");
}

// SuddenQuiz03: Sudden Quiz taken on 10/15/2019
void SuddenQuiz03() {
	int width = 6; // Default: 6

	width = GetNumericAnswer("Enter Size of the Dynamic Z ->", width);

	for (int lineRow = 0; lineRow <= width; lineRow++) {
		if (lineRow == 0 || lineRow == width) {
			for (int lineSpace = width; lineSpace > 0; lineSpace--) {
				PrintLine("O ", false);
			}
		}
		else {
			int positionToPlace = width - lineRow;
			if (positionToPlace != 1) {
				while (positionToPlace > 1) {
					PrintLine("  ", false);
					positionToPlace--;
				}
				PrintLine("O ", false);
			}
		}
		if ((width - lineRow) != 1) {
			PrintLine();
		}

	}
}

// SuddenQuiz04: Sudden Quiz taken on 10/24/2019 (PrintWords: Print Word Count and occurance of characters for Sentence Inputted)
void SuddenQuiz04() {
	// 96 Subtracted from ASCII if Lowercase; 64 if Uppercase

	string input = "";
	int wordCount = 0;
	int totalCharacterIndex = 0;
	int tempVariable = 0;

	input = ToLowerCase(GetTextAnswer("Input a Sentence -> ", input));

	const char* str = input.c_str(); // Character Array Mapping, * is similar to a dynamic memory here
	map<char, int> count; // Stores the Character within the Alphabet alongside
	map<int, int> charIntIndex; // Stores a word and Number of characters

	// Size_T instead of Int is used here to avoid Error C4018: signed/unsigned mismatch
	for (size_t i = 0; i < input.size() && input[i] != '\0'; i++)
	{
		if (input[i] == ' ') {
			// Store tempVariable in charIntIndex, then reset it
			charIntIndex[wordCount] = tempVariable;
			tempVariable = 0;

			// Add a word if space detected
			wordCount++;
		}
		else {
			// Add to Word Occurances
			count[str[i]]++;

			// Add Character Value to temp Variable
			tempVariable += (str[i] - 96);
		}
	}

	if (tempVariable > 0) {
		// Store final tempVariable in charIntIndex, then reset it
		charIntIndex[wordCount + 1] = tempVariable;
		tempVariable = 0;
	}
	PrintLine("Number of Words: " + to_string(wordCount + 1));

	// Output the Character and Amount of Occurances
	// Auto is Similar to Type T in Java, which means it'll auto assign dynamically
	// Based on the circumstances (In this case: char and int)
	for (const auto mapping : count) {
		PrintLine("The letter (" + (ToUpperCase(CharacterToString(mapping.first)) + " or " + ToLowerCase(CharacterToString(mapping.first))) + ") occurs a total of " + to_string(mapping.second) + " times");
	}

	// Calculate Total Character Index by
	// Adding all Data from map then use countDigits
	// to get the sum of the digits at the end
	// Then output totalCharacterIndex
	for (const auto charIndex : charIntIndex) {
		totalCharacterIndex += charIndex.second;
	}
	totalCharacterIndex = CountDigits(totalCharacterIndex);
	PrintLine("Total Character Index: " + to_string(totalCharacterIndex));
}

// GetExpressionNumber: Retrieve an Expression Number based on Character
int GetExpressionNumber(char inputCharacter) {
	switch (inputCharacter) {
	case 'a' | 'A':
	case 'j' | 'J':
	case 's' | 'S':
		return 1;
	case 'b' | 'B':
	case 'k' | 'K':
	case 't' | 'T':
		return 2;
	case 'c' | 'C':
	case 'l' | 'L':
	case 'u' | 'U':
		return 3;
	case 'd' | 'D':
	case 'm' | 'M':
	case 'v' | 'V':
		return 4;
	case 'e' | 'E':
	case 'n' | 'N':
	case 'w' | 'W':
		return 5;
	case 'f' | 'F':
	case 'o' | 'O':
	case 'x' | 'X':
		return 6;
	case 'g' | 'G':
	case 'p' | 'P':
	case 'y' | 'Y':
		return 7;
	case 'h' | 'H':
	case 'q' | 'Q':
	case 'z' | 'Z':
		return 8;
	case 'i' | 'I':
	case 'r' | 'R':
		return 9;
	default:
		return 0; // 0 = Invalid Value
	}
}

// SuddenQuiz05: Sudden Quiz taken on 10/30/2019 (DetermineExpressionNumber: Retrieves an Expression Number from your Name)
void SuddenQuiz05() {
	string input = "";
	int tempVariable = 0, expressionNumber = 0;

	input = ToLowerCase(GetTextAnswer("Input your Name (Without Suffix/Prefix) -> ", input));

	const char* str = input.c_str(); // Character Array Mapping, * is similar to a dynamic memory here

	// Size_T instead of Int is used here to avoid Error C4018: signed/unsigned mismatch
	for (size_t i = 0; i < input.size() && input[i] != '\0'; i++)
	{
		if (input[i] == ' ') {
			// Space Detected, store tempVariable into final count and reset it

			// If not a Master Number (11, 22, 33, etc), reduce Number with CountDigits
			if (tempVariable % 11 != 0) {
				tempVariable = CountDigits(tempVariable);
			}

			expressionNumber += tempVariable;
			tempVariable = 0;
		}
		else {
			// For Each Letter, Add Expression Number of that Letter to Temp Variable
			tempVariable += GetExpressionNumber(str[i]);
		}
	}

	// Add Remainder of tempVariable
	if (tempVariable % 11 != 0) {
		tempVariable = CountDigits(tempVariable);
	}

	expressionNumber += tempVariable;
	tempVariable = 0;

	// Add Up Using same Master Number Calculation as above, but for final number
	// Also Sum Down CountDigits to One Digit Sum
	if (expressionNumber % 11 != 0) {
		expressionNumber = CountDigits(expressionNumber, true);
	}

	PrintLine("Expression Number: " + to_string(expressionNumber));
}

// GetPossibleMatchingTritaxagonialNumbers: Return the Next Matching triagonal, Pentagonal, and Hexagonal Number after beginningIndex
void GetPossibleMatchingTritaxagonialNumbers(int beginningIndex = 1, int maximumIndex = MAXIMUM_INT, bool toggleLogging = false) {
	// Enable Debug Logging, if toggled
	debugLogging = toggleLogging;

	long long nextHexagonalNumber = 0;
	int foundIndex = beginningIndex;

	string continueExecutionInput = "";
	bool continueExecution = false;

	for (int i = beginningIndex; i < maximumIndex; i++) {
		// Only Odd Numbers will work in all three Categories, this IF prevents any memory leaks and uneede computations
		if (i % 2 != 0) {
			// Generate Hexagonal Number for Index
			// Note: Hexagonal Number set is a subset to Triagonal Numbers, so each hexagonal Number is also a Triagonal Number
			// 3rd Hexagonal Number == 5th Triagonal Number as Reference
			nextHexagonalNumber = (long long)i * ((long long)2 * i - 1);

			// If the Generated Hexagonal is also a Pentagonal Number, break out of the loop
			if (GetPentagonal(nextHexagonalNumber).first) {
				foundIndex = i;
				break;
			}
		}
	}

	// Disable Excess Logging if Enabled for this Method
	if (toggleLogging) {
		debugLogging = false;
	}

	PrintLine("Found Matching Number at Hexagonal Index: " + to_string(foundIndex));
	PrintLine("Trigonal Equivalent Index: " + to_string(foundIndex + (foundIndex - 1)));
	PrintLine("Matching Number: " + to_string(nextHexagonalNumber));

	continueExecutionInput = GetTextAnswer("Continue Method Execution (Y/N) -> ", continueExecutionInput);
	continueExecution = continueExecutionInput == "Y";

	if (continueExecution && foundIndex + 1 < maximumIndex) {
		// Restarts Method at lastFoundIndex + 1
		GetPossibleMatchingTritaxagonialNumbers(foundIndex + 1, maximumIndex, toggleLogging);
	}
}

// SuddenQuiz06: Sudden Quiz taken on 11/06/2019
void SuddenQuiz06() {
	int startIndex = 0, maxIndex = MAXIMUM_INT;

	startIndex = GetNumericAnswer("Input Starting Index to Check Matching Triagonal, Pentagonal, and Hexagonal Numbers (1 or Higher) -> ", startIndex);
	maxIndex = GetNumericAnswer("Input Maximum Index to Check Matching Triagonal, Pentagonal, and Hexagonal Numbers -> ", maxIndex);

	if (startIndex >= maxIndex) {
		startIndex = maxIndex - 1;
	}

	if (startIndex < 1) {
		startIndex = 1;
	}

	GetPossibleMatchingTritaxagonialNumbers(startIndex, maxIndex, true);
}

// CalculateMatchingPentagonals: Determine for each value until maxIndex within distance if Sum and Difference are both Pentagonal's
void CalculateMatchingPentagonals() {
	// Note: Distance can also be known as d
	// Note: n is the distance between it's index and the maxIndex
	int maxIndex = 0, distance = 0;
	
	maxIndex = GetNumericAnswer("Input Maximum Index to Check Pentagonals towards -> ", maxIndex);
	distance = GetNumericAnswer("Input Beginning Index to check from (Default: 0) -> ", distance);

	// Check Pentagonal Numbers over Distance

	// Note: GetPentagonal is stored as an auto, as my method returns a Tuple of bool
	// and the pentagonal number it returns if the first bool is true
	for (int n = distance + 1; n <= maxIndex; n++)
	{
		auto p_n = GetPentagonal(n, true).second;

		auto x = n - distance;
		auto p_x = GetPentagonal(x, true).second;

		// Check Sum and Difference
		auto sum = p_n + p_x;
		auto sumPentagonalData = GetPentagonal(sum);

		auto difference = p_n - p_x;
		auto differencePentagonalData = GetPentagonal(difference);

		// If Both the Sum and Difference are Pentagonals, print it out!
		if (sumPentagonalData.first || differencePentagonalData.first) {
			PrintLine("Found Matching Pentagonal Number from Sum and Difference of " + to_string(p_n) + " and " + to_string(p_x) + " as " + to_string(p_n) + " with distance (D) of " + to_string(distance) + " and current index (N) of " + to_string(n));
		} else {
			PrintLine("No Matching Pentagonal Numbers from Sum and Difference " + to_string(p_n) + " and " + to_string(p_x) + " with distance (D) of " + to_string(distance) + " and current index (N) of " + to_string(n));
		}
	}
}

// Exam01: Exam BB Questions done on 10/8/2019
void Exam01() {
	// Q10 Data
	int lengthForSides = 3, maxStars = 4, sides = 2; // 3 Rows on each side (2)

	// Decrease stars as going towards the middle
	for (int currentRow = lengthForSides * 2; currentRow >= 0; currentRow--) {
		for (int currentRowArea = maxStars; currentRowArea > 0; currentRowArea--) {
			PrintLine("* ", false);
		}

		if (currentRow <= lengthForSides) {
			if (currentRow == lengthForSides + 1) {
				// Middle Row
				PrintLine("*");
			}
			maxStars++;
		}
		else {
			maxStars--;
		}

		PrintLine();
	}
}

// ConeData: Calculate the volume and surface area of a Cone using PI, radius, slant height, and the cone's height
void ConeData() {
	double inputHeight = 0, inputRadius = 0, slantHeight;
	double surfaceArea, volume;

	inputHeight = GetNumericAnswer("Input Height of Cone: ", inputHeight);
	inputRadius = GetNumericAnswer("Input Radius of Cone: ", inputRadius);

	slantHeight = sqrt(pow(inputRadius, 2) + pow(inputHeight, 2));
	surfaceArea = PI * (inputRadius * (inputRadius + slantHeight)); // PI * radius(radius + slantHeight)
	volume = PI * pow(inputRadius, 2) * (inputHeight / 3);

	PrintLine("The volume of a cone with a radius of " + to_string(inputRadius) + " and height of " + to_string(inputHeight) + " is (" + to_string(volume) + ")");
	PrintLine("The surface area of a cone with a radius of " + to_string(inputRadius) + " and height of " + to_string(inputHeight) + " is (" + to_string(surfaceArea) + ")");
}

// ConditionalTest: Boolean and If-Case Testing done on 09/17/2019 - Refactored on 09/26/2019
void ConditionalTest() {
	const int maxTests = 3;
	double a = 0, b = 0, c = 0, output = 0;
	int a_Int = 0, s = 0, selection = 0;

	string resultOutput = "Invalid Data, try again...";

	selection = GetNumericAnswer("Which Test do you wish to do? ", selection);

	// Example 1 Data
	if (selection == 0) {
		a = GetNumericAnswer("Enter a Number for A: ", a);
		b = GetNumericAnswer("Enter a Number for B: ", b);
		c = GetNumericAnswer("Enter a Number for C: ", c);
		s = GetNumericAnswer("Enter an Integer for S: ", s);

		if (s == 1) {
			output = a + b + c;
		}
		else if (s == 2) {
			output = (b + c) / a;
		}
		else if (s == 3) {
			output = a - b;
		}
		else if (s == 4) {
			output = a * b * c;
		}
		else if (s == 5) {
			output = a / b;
		}
		else if (s == 6) {
			output = int(a) % int(b);
		}
		else if (s == 7) {
			// NULL DATA
		}
		else if (s == 8) {
			output = (a + b) / 10;
		}
		else if (s == 9) {
			output = (a / b) * 100;
		}

		resultOutput = (output != 0 ? to_string(output) : "Wrong Input Entered, try again...");
	}
	else if (selection == 1) {
		// Example 2 Data
		a_Int = GetNumericAnswer("Enter an Integer for A: ", a_Int);

		if (a_Int != 0) {
			resultOutput = ((a_Int % 2 == 0) ? "Even" : "Odd") + a_Int > 0 ? " Positive" : " Negative";
		}
		else {
			resultOutput = "Zero";
		}
	}

	PrintLine("Output: " + resultOutput);
}

// SwitchTest: Switch Case Testing done on 09/19/2019
void SwitchTest() {
	double a = 0, b = 0, c = 0, output = 0;
	char H = ' ';
	int s = 0;

	a = GetNumericAnswer("Enter a Number for A: ", a);
	b = GetNumericAnswer("Enter a Number for B: ", b);
	c = GetNumericAnswer("Enter a Number for C: ", c);
	s = GetNumericAnswer("Input an Integer for S: ", s);
	H = GetCharacterAnswer("Input Selection for H: ", H);

	switch (s) {
	case 1:
		output = a + b + c;
		break;
	case 2:
		output = (b + c) / a;
		break;
	case 3:
		output = a / b;
		break;
	case 4:
		output = (a + b + c) / 5;
		break;
	case 5:
		output = 5 * a + 6 * c;
		break;
	case 6:
		output = 7 * a / b;
		break;
	case 7:
		output = (7 * a + c) / 5;
		break;
	case 8:
		output = (int)a % (int)b;
		break;
	case 9:
		output = a - b - c;
		break;
	default:
		PrintLine("Others");
	}

	if (H == 'A' || H == 'a') {
		PrintLine(to_string(output));
	}
	else {
		PrintLine(to_string(10 * output));
	}
}

// MonthlyWage: Calculating Regular, Overtime, and Triple Payouts for Pay Rate and Hours Worked
void MonthlyWage() {
	double workedHours = 0, hourlyPayRate = 0;

	double normalPay = 0, overtimePay = 0, triplePay = 0, totalWeeklyPay = 0, totalMonthlyPay = 0;

	workedHours = GetNumericAnswer("Input Hours worked in a week -> ", workedHours);
	hourlyPayRate = GetNumericAnswer("Input Pay Rate per hour -> ", hourlyPayRate);

	double hoursOfOvertime = (workedHours > 40) ? (workedHours <= 60 ? workedHours : 60) - 40 : 0,
		hoursOfTriplePay = (workedHours > 60) ? workedHours - 60 : 0,
		normalHours = workedHours - hoursOfOvertime - hoursOfTriplePay;

	overtimePay = (1.75 * hourlyPayRate) * hoursOfOvertime;
	triplePay = (3 * hourlyPayRate) * hoursOfTriplePay;
	normalPay = hourlyPayRate * normalHours;

	totalWeeklyPay = normalPay + overtimePay + triplePay;
	totalMonthlyPay = totalWeeklyPay * 4;

	PrintLine("Regular Pay: \t $" + to_string(normalPay));
	PrintLine("Overtime Pay: \t $" + to_string(overtimePay));
	PrintLine("Triple Pay: \t $" + to_string(triplePay));

	PrintLine("\nTotal Pay (Weekly): \t $" + to_string(totalWeeklyPay));
	PrintLine("Total Pay (Monthly): \t $" + to_string(totalMonthlyPay));
}

// ConditionalTest2: Secondary Testing of If-Else Statements done on 09/24/2019
void ConditionalTest2() {
	int a = 0;
	bool evenNum = false;

	a = GetNumericAnswer("Input a Number: ", a);
	evenNum = a % 2 == 0;

	if (a < 100) {
		if (a < 50) {
			PrintLine(evenNum ? "apple" : "carrot");
		}
		else {
			PrintLine(evenNum ? "orange" : "eggplant");
		}
	}
	else {
		if (a > 150) {
			PrintLine(evenNum ? "watermelon" : "potato");
		}
		else {
			PrintLine(evenNum ? "papaya" : "tomato");
		}
	}
}

// Grade: Outputs a Grade Letter based on Percentage
void Grade() {
	float a = 0, totalNumber = 100, initialNumber = 0;
	string gradeLetter = "F";

	initialNumber = GetNumericAnswer("Input the total points you received: ", initialNumber);
	totalNumber = GetNumericAnswer("Input the total grade points (Leave Blank for 100): ", totalNumber);

	a = initialNumber / totalNumber;

	gradeLetter = (a >= 90) ? "A" :
		(a >= 80) ? "B" :
		(a >= 70) ? "C" :
		(a >= 60) ? "D" : "F";


	PrintLine("Total: " + to_string(initialNumber) + "/" + to_string(totalNumber) + "(" + to_string(a) + " - " + gradeLetter + ")");
	PrintLine("Your GPA is " + to_string(a / 25));
}

// LoopTests: Loop Testing done on 09/26/2019 (Alternate Name: SequenceDesigner)
void LoopTests() {
	double startNumber = 0, maxNumber = 0, currentSum = 0, countBy = 0, powerMode = 0, powerAmount = 0;
	bool staticSequence = false;
	string optionalOutput = "";

	staticSequence = ToLowerCase(GetTextAnswer("Enable Static Sequence Mode (Printing a Seperated Sequence of numbers) -> ", optionalOutput)) == "true";

	startNumber = GetNumericAnswer("Enter a Number for the Start Number: ", startNumber);
	maxNumber = GetNumericAnswer("Enter a Number for the Number to Count towards: ", maxNumber);
	countBy = GetNumericAnswer("Enter a Number to select mode of counting above 1 (0 for Normal): ", countBy);
	powerMode = GetNumericAnswer("Enter a power mode, or press 0 for normal (1 for within, 2 for after): ", powerMode);
	powerAmount = GetNumericAnswer("Enter a power amount, or presss 0 for None: ", powerAmount);

	startNumber = (startNumber >= maxNumber) ? startNumber - 1 : startNumber;

	for (double currentNumber = startNumber; currentNumber <= maxNumber; currentNumber += (countBy > 0) ? countBy : 1) {
		currentSum += (powerMode == 1) ? pow(currentNumber, powerAmount) : currentNumber;
		if (staticSequence) {
			optionalOutput += to_string(currentNumber) + ((currentNumber >= maxNumber) ? "" : ", ");
		}
		else {
			PrintLine("Current Sum at " + to_string(currentNumber) + ": " + to_string(currentSum));
		}
	}

	if (powerMode == 2) {
		currentSum = pow(currentSum, powerAmount);
	}

	PrintLine("Result: Successfully Counted/Added from " + to_string(startNumber) + " to " + to_string(maxNumber));

	if (!IsNullOrEmpty(optionalOutput)) {
		PrintLine("Output: { " + optionalOutput + " }");
	}
}

// LoopTests2: Secondary Loop Testing done on 10/3/2019
void LoopTests2() {
	int sidedLength = 4;

	// Q4/Q5
	sidedLength = GetNumericAnswer("Set the Length for the Maximum sided Length: ", sidedLength);

	// Q1 (Defunct)
	/*int divider = 4, maxLines = 8;
	for (int i = 0; i <= maxLines; i++) {
		int printedLines = 0;

		while (printedLines != i && divider != 0) {
			PrintLine("*", false);
			if (printedLines <= divider) {
				divider--;
			}
			printedLines++;
		}
		PrintLine("");
	}*/

	// Q2
	for (int i = 0; i < sidedLength; i++) {
		int spaces = 0;

		// Q4
		int excess = i;

		while (spaces < sidedLength - (i + 1)) {
			PrintLine(" ", false);
			spaces++;
		}

		while (spaces < sidedLength) {
			PrintLine("*", false);
			spaces++;
		}

		// Q4
		while (excess != 0) {
			PrintLine("*", false);
			excess--;
		}

		PrintLine("");
	}

	// Q4 Middle
	int maxLength = sidedLength * 2;
	while (maxLength != 0) {
		PrintLine((maxLength % 2 == 0) ? ">" : "<", false);
		maxLength--;
	}
	PrintLine("");

	// Q3
	for (int i = 0; i < sidedLength; i++) {
		int spaces = i;
		int remaining = sidedLength - i;

		// Q4
		int excess = remaining - 1;

		while (spaces != 0) {
			PrintLine(" ", false);
			spaces--;
		}

		while (remaining != 0) {
			PrintLine("^", false);
			remaining--;
		}

		// Q4
		while (excess != 0) {
			PrintLine("^", false);
			excess--;
		}

		PrintLine("");
	}
}

// DayOfBirthWeek: Calculates and Outputs the Day of the Week of Birth
void DayOfBirthWeek() {
	int inputDay = 0, inputMonth = 0, inputYear_LastTwo = 0, inputYear_FirstTwo = 0, inputFullYear = 0; // User Inputs

	bool isLeapYear = false;
	int total = 0;

	string outputMonth = "", outputDayOfWeek = "";

	inputMonth = GetNumericAnswer("Input Month (Format: XX or X) -> ", inputMonth);
	inputDay = GetNumericAnswer("Input Day (Format: XX or X) -> ", inputDay);
	inputFullYear = GetNumericAnswer("Input Full Year (Format: XXXX) -> ", inputFullYear);

	inputYear_FirstTwo = (inputFullYear / 100);
	inputYear_LastTwo = (inputFullYear % 100);

	// Determine if Inputted Year is a leap year
	// A. Divisible by 4
	// B. NOT divisible by 100 OR is also divisible by 400
	isLeapYear = (inputFullYear % 4 == 0) && (inputFullYear % 100 != 0 || inputFullYear % 400 == 0);

	// Getting the Month Number and Adding to Total
	total += (inputYear_LastTwo / 4) + inputYear_LastTwo + inputDay; // (Last 2 Digits of Birth Year / 4) + 2 Digit Birth Year + Two Digit Birth Day

	// Set OutputMonth and Add to total depending on Inputted Month
	switch (inputMonth) {
	case 1:
		total += 1;
		outputMonth = "January";
		break;
	case 2:
		total += 4;
		outputMonth = "February";
		break;
	case 3:
		total += 4;
		outputMonth = "March";
		break;
	case 4:
		//total += 0;
		outputMonth = "April";
		break;
	case 5:
		total += 2;
		outputMonth = "May";
		break;
	case 6:
		total += 5;
		outputMonth = "June";
		break;
	case 7:
		//total += 0;
		outputMonth = "July";
		break;
	case 8:
		total += 3;
		outputMonth = "August";
		break;
	case 9:
		total += 6;
		outputMonth = "September";
		break;
	case 10:
		total += 1;
		outputMonth = "October";
		break;
	case 11:
		total += 4;
		outputMonth = "November";
		break;
	case 12:
		total += 6;
		outputMonth = "December";
		break;
	default:
		outputMonth = ""; // Default: Invalid Value
	}


	// IF Leap year and Month is January or February subtract 1 from total
	if (isLeapYear && (inputMonth == 1 || inputMonth == 2)) {
		total -= 1;
	}

	int a = inputYear_FirstTwo % 4;
	total += (a == 0) ? 6 :
		(a == 1) ? 4 :
		(a == 2) ? 2 :
		0; // Default: If a is an invalid value

	// Warn against determining the day of birth for years earlier then 1900
	if (inputYear_FirstTwo < 19) {
		PrintLine("Warning: Advisory against using Day Determination for Years earlier then 1900");
	}

	// Find, Calculate and Output the Final Day
	int b = total % 7;
	outputDayOfWeek = (b == 1) ? "Sunday" :
		(b == 2) ? "Monday" :
		(b == 3) ? "Tuesday" :
		(b == 4) ? "Wednesday" :
		(b == 5) ? "Thursday" :
		(b == 6) ? "Friday" :
		(b == 0) ? "Saturday" :
		""; // Default: Invalid Value

	PrintLine("A person born on " + outputMonth + " " + to_string(inputDay) + ", " + to_string(inputFullYear) + " was born on a " + outputDayOfWeek + ", and " + (isLeapYear ? "is" : "is not") + " a leap year.");
}

// PerfectSquare_4Digit: Calculates whether an inputted 4-digit number is a perfect square
void PerfectSquare_4Digit() {
	bool automaticMode = false;
	string automaticModeInput = "";

	int inputNumber = 0;
	double squareRooted = 0, sumMultiplied = 0;

	automaticMode = ToLowerCase(GetTextAnswer("Use Automatic Detection? (Y/N) -> ", automaticModeInput)) == "y";

	if (automaticMode) {
		for (inputNumber = 32; inputNumber <= 99; inputNumber++) {
			sumMultiplied = (double)inputNumber * (double)inputNumber;

			// If Sum of digitsAdded is the square root of inputNumber, thus having sumMultiplied equal the inputNumber
			// Then inputNumber is a 4-digit perfect square
			PrintLine("The result of " + to_string(inputNumber) + ": (" + to_string(sumMultiplied) + ") is a 4-digit perfect square!");
		}
	}
	else {
		inputNumber = GetNumericAnswer("Input a 4 Digit Number -> ", inputNumber);

		squareRooted = sqrt(inputNumber);
		sumMultiplied = squareRooted * squareRooted;

		// If Sum of digitsAdded is the square root of inputNumber, thus having sumMultiplied equal the inputNumber
		// Then inputNumber is a 4-digit perfect square
		PrintLine("The number " + to_string(inputNumber) + " " + ((sumMultiplied == inputNumber) ? "is" : "is not") + " a 4-digit perfect square!");
	}
}

// StairCreator: Creates a Set of Stairs with a specified Row Count and interchangable characters
void StairCreator() {
	int rows = 0;
	string character1 = "$", character2 = "#";
	bool interchangeCharacters = false, usingSecondCharacter = false, spaceBetweenLines = true, spaceBetweenCharacters = false;

	rows = GetNumericAnswer("Input Amount of Rows to Generate -> ", rows);

	if (ToLowerCase(GetTextAnswer("Do you wish to Customize Options (Y/N) -> ", character1)) == "y") {
		// Configuration Options
		interchangeCharacters = ToLowerCase(GetTextAnswer("Do you wish to Interchange Characters between Rows (Y/N) (Default: N) -> ", character1)) == "y";
		spaceBetweenLines = ToLowerCase(GetTextAnswer("Do you wish to have an Empty Line (A Space) between each line (Y/N) (Default: Y) -> ", character1)) == "y";
		spaceBetweenCharacters = ToLowerCase(GetTextAnswer("Do you wish to have a space between each character in a line (Y/N) (Default: N) -> ", character1)) == "y";

		// Customizing Characters
		if (interchangeCharacters) {
			if (ToLowerCase(GetTextAnswer("Do you wish to Customize Characters to Generate (Y/N) -> ", character1)) == "y") {
				character1 = GetTextAnswer("Input First Character (Default: $) -> ", character1);
				character2 = GetTextAnswer("Input Second Character (Default: #) -> ", character2);
			}
		}
	}

	// LOOP GENERATION
	for (int currentRow = 1; currentRow <= rows; currentRow++) {
		for (int currentPosition = 1; currentPosition <= currentRow; currentPosition++) {
			PrintLine((usingSecondCharacter ? character2 : character1) + ((spaceBetweenCharacters) ? " " : ""), false);
		}

		if (spaceBetweenLines) {
			PrintLine();
		}
		if (interchangeCharacters) {
			usingSecondCharacter = !usingSecondCharacter;
		}
	}

	PrintLine("\n Stair Generation Completed Successfully!");
}

// ArrayTesting: Array Testing done on 10/29/2019
void ArrayTesting() {
	const int maxAllowedElements = 1000, minimumAllowedElements = 2;

	int a[maxAllowedElements]; // Maximum of 1000 Inputs
	int n = 0;
	string sortChoice = "";

	sortChoice = GetTextAnswer("Choose Sorting Method (ascending/descending) -> ", sortChoice);
	n = GetNumericAnswer("Enter How many Elements to sort -> ", n);

	// If Elements to Sort is greater then the max allowed, change to fit into max
	if (n > maxAllowedElements) {
		n = maxAllowedElements;
	}
	else if (n < minimumAllowedElements) {
		n = minimumAllowedElements;
	}

	for (int i = 0; i < n; i++) {
		a[i] = i; // Default Value is Element Number
		a[i] = GetNumericAnswer("Input Element " + to_string(i + 1) + ": ", a[i]);
	}

	PrintLine("Original Sequence: ");
	for (int i = 0; i < n; i++) {
		PrintLine("Element " + to_string(i + 1) + ": " + to_string(a[i]));
	}

	//...........................
	// Sort Depending on Choice (Using the Algorithm Defines)

	// Descending Mode
	if (ToLowerCase(sortChoice) == "descending") {
		sort(begin(a), end(a), greater<>());
	}
	else {
		// Ascending Order
		for (int j = n - 1; j >= 0; j--) {
			for (int i = 0; i < j; i++) {
				int temp;

				if (a[i] > a[i + 1]) {
					temp = a[i];

					a[i] = a[i + 1];
					a[i + 1] = temp;
				}
			}
		}
	}

	//...........................

	PrintLine("Sorted Sequence: ");
	for (int i = 0; i < n; i++) {
		PrintLine("Element " + to_string(i + 1) + ": " + to_string(a[i]));
	}
}

// FileInputTesting: File Input Testing done on 11/19/2019
void FileInputTesting() {
	int totalUniqueCount = 0;
	vector<string> inputData;
	map<string, int> lineOccurances;

	GetFileLines("input.txt", inputData);

	for (const auto& currentLine : inputData) {
		lineOccurances[currentLine] += 1;
	}

	for (const auto mapping : lineOccurances) {
		PrintLine("Number of " + mapping.first + " in document: " + to_string(mapping.second));
		totalUniqueCount += 1;
	}

	PrintLine("Total Number of Unique Lines in Document: " + to_string(totalUniqueCount));
}

// FileOutputTesting: File Output Testing done on 11/21/2019
void FileOutputTesting() {
	int maxLines = 0;
	maxLines = GetNumericAnswer("Please Enter Total Line Count to add -> ", maxLines);

	vector<string> outputData;

	for (int currentIndex = 0; currentIndex < maxLines; currentIndex++) {
		string newLine = "";
		newLine = GetTextAnswer("Input Line Data for Line " + to_string(currentIndex + 1) + " -> ", newLine);

		outputData.push_back(newLine);
	}

	SaveFileLines("my1.txt", outputData);

	PrintLine("Operation Completed!");
}

// BMIFileTesting: Reading Inputs of {name, weight in pounds, height in inches} to output to a new document as {name: calculatedBMI}
void BMIFileTesting() {
	vector<string> inputData, outputData;

	GetFileLines("bmiIn.txt", inputData);

	for (size_t currentLine = 0; currentLine < inputData.size(); currentLine += 3) {
		// For every 3 Lines, Format a Output Line with the following Format:
		// {Name: calculatedBMI}
		double calculatedBMI = (703 * stod(inputData[currentLine + 2])) / (pow(stod(inputData[currentLine + 1]), 2));

		outputData.push_back(inputData[currentLine] + "'s BMI: " + to_string(calculatedBMI));
	}

	SaveFileLines("bmiOut.txt", outputData);

	PrintLine("Operation Completed!");
}

// ProfileDatabase: Profile Data outputted to a file, done on 11/26/2019
void ProfileDatabase() {
	vector<string> fnames, lnames, dobs, ssns, foods, games, visits, heights, weights;

	vector<string> totalData;

	int maxIndex = 0;
	maxIndex = GetNumericAnswer("How many People are you gaining info on -> ", maxIndex);

	for (int originalIndex = 0; originalIndex < maxIndex; originalIndex++) {
		string emptyData = "";
		int currentIndex = originalIndex + 1;

		PrintLine("Setting Up Profile for Person #" + to_string(currentIndex) + "\n");

		fnames.push_back(GetTextAnswer("What is Person #" + to_string(currentIndex) + "'s First Name -> ", emptyData));
		lnames.push_back(GetTextAnswer("What is Person #" + to_string(currentIndex) + "'s Last Name -> ", emptyData));
		dobs.push_back(GetTextAnswer("What is Person #" + to_string(currentIndex) + "'s DOB (As MM/DD/YYYY) -> ", emptyData));
		ssns.push_back(GetTextAnswer("What is Person #" + to_string(currentIndex) + "'s SSN -> ", emptyData));
		foods.push_back(GetTextAnswer("What is Person #" + to_string(currentIndex) + "'s Favorite Food -> ", emptyData));
		games.push_back(GetTextAnswer("What is Person #" + to_string(currentIndex) + "'s Favorite Game to Play -> ", emptyData));
		visits.push_back(GetTextAnswer("What is Person #" + to_string(currentIndex) + "'s Favorite Place to Visit -> ", emptyData));
		heights.push_back(GetTextAnswer("What is Person #" + to_string(currentIndex) + "'s Height (In Centimeters) -> ", emptyData));
		weights.push_back(GetTextAnswer("What is Person #" + to_string(currentIndex) + "'s Weight (In Pounds) -> ", emptyData));

		// Add to Total Data
		totalData.push_back("Profile for Person #" + to_string(currentIndex) + ":");
		totalData.push_back("\t First Name - " + fnames[originalIndex]);
		totalData.push_back("\t Last Name - " + lnames[originalIndex]);
		totalData.push_back("\t Date of Birth - " + dobs[originalIndex]);
		totalData.push_back("\t SSN - " + ssns[originalIndex]);
		totalData.push_back("\t Favorite Food - " + foods[originalIndex]);
		totalData.push_back("\t Favorite Game - " + games[originalIndex]);
		totalData.push_back("\t Favorite Place to Visit - " + visits[originalIndex]);
		totalData.push_back("\t Height (In Centimeters) - " + heights[originalIndex]);
		totalData.push_back("\t Weight (In Pounds) - " + weights[originalIndex]);
		totalData.push_back("=====================");
	}

	SaveFileLines("profileData.txt", totalData);
	PrintLine("Operation Completed!");
}

// CountOfSundays: Determine the Number of Sundays from an Origin to Destination Data (TODO: Expand to any Weekday)
void CountOfSundays() {
    long long yearFrom = 0, yearTo = 0;
    int monthFrom = 0, monthTo = 0, dayFrom = 0, dayTo = 0;
    
    yearFrom = GetNumericAnswer("Input Origin Year (As XXXX) -> ", yearFrom);
    yearTo = GetNumericAnswer("Input Destination Year (As XXXX) -> ", yearTo);
    
    monthFrom = GetNumericAnswer("Input Origin Month (As XX from 01-12) -> ", monthFrom);
    monthTo = GetNumericAnswer("Input Destination Month (As XX from 01-12) -> ", monthTo);
    
    dayFrom = GetNumericAnswer("Input Origin Day (As XX from 01-31) -> ", dayFrom);
    dayTo = GetNumericAnswer("Input Destination Day (As XX from 01-31) -> ", dayTo);
        
    // Arrange From -> To Data from Largest to Smallest, to avoid user error
    if (yearTo < yearFrom || (yearTo == yearFrom && monthTo < monthFrom))
    {
        std::swap(yearFrom,  yearTo);
        std::swap(monthFrom, monthTo);
    }
        
    // Begin at the First Day of the Month and Iterate through the timeline
    long long currentYear  = yearFrom;
    int       currentMonth = monthFrom;
    if (dayFrom > 1)
    {
        currentMonth++;
        
        // Iterate from December to January of next year
        // Reset Current Month at next year
        if (currentMonth > 12)
        {
            currentMonth -= 12;
            currentYear++;
        }
    }
        
    // The Number of Sundays that Match
    int sum = 0;
        
    // Iterate the Same Pattern every 2800 Years, to prevent an anomaly
    // Note: For every 2800 Years there are 4816 Sundays
    while (currentYear + 2800 < yearTo)
    {
        currentYear += 2800;
        sum += 4816;
    }
    
    // simple scan through all months
    while (currentMonth < monthTo || currentYear < yearTo) // days already match, they are both 1
    {
        // Count Sundays as they come (Sunday = 1 in GetDayOfWeek)
        if (GetDayOfWeek(currentYear, currentMonth, 1) == 1)
            sum++;
            
            currentMonth++;
        
            // from December to January of next year
            if (currentMonth > 12)
            {
                currentMonth -= 12;
                currentYear++;
            }
    }
    
    // Ensure the Last Month is also checked (Sunday = 1 in GetDayOfWeek
    if (GetDayOfWeek(currentYear, currentMonth, 1) == 1)
        sum++;
        
    // Print Results
    PrintLine("Count of Sundays from Origin to Destination -> " + to_string(sum));
}

// SuddenQuiz08: Sudden Quiz 8 done on 12/10/2019 (DigitPowers: The Sum of all Numbers that can be written as the sum of powers of their digits)
void SuddenQuiz08() {
	// Warn the User of Performance Overhead
	PrintLine("Method Warning: This Method, as defined in Euler, has significant performance overhead. Caution Advised!");

	int powerExponent = 0, totalSum = 0;
	powerExponent = GetNumericAnswer("Input Power Amount to interpret sums of numbers from -> ", powerExponent);

	// Ensures a Number cannot be above or at 8 digits
	// So If all digits are 9s, then
	// 7 Digits -> 7 * 9^6 -> 3720087 Maximum Number
	// 8 Digits -> 8 * 9^6 -> 4251528 Maximum Number
	for (int currentNumber = 2; currentNumber <= (7 * pow(9, 6)); currentNumber++) {
		double currentExponentSum = 0;

		// Removes the Right-Most Digit,
		// reducing and splitting the number to it's final digit
		int reducedNumber = currentNumber;
		while (reducedNumber > 0) {
			int digitToRemove = reducedNumber % 10;
			reducedNumber /= 10;

			// Get the Exponential Powered Number for the Digit, then add to Sum
			currentExponentSum += pow(digitToRemove, powerExponent);
		}

		// If Sum is equal to original Number, Add Original Number
		if (currentExponentSum == currentNumber) {
			totalSum += currentNumber;
		}
	}

	// Print Final Output
	PrintLine("Result -> " + to_string(totalSum));
}

// FactorialSums: Sudden Quiz 09 taken on 12/12/2019, used for calculating the sum of numbers equal to the factorial sums
void FactorialSums() {
	// Precomputed Mapping of Factorials from 0! to 9! -- Used to save performance
	// Generated by the Following (Psudo-Code Example):
	//
	//int constFactorials[] = { 1 };
	//for (int i = 1; i < 10; i++)
	//	constFactorials[i] = constFactorials[i - 1] * i;
	const int constFactorials[] = { 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880 };

	// Forbid Numbers above the Below Limit, uncomment cout and cin to specify limit
	// Default is 2540160 -> No more then 7 Digit Sums for Original Problem
	int factorSumLimit = 2540160;
	//cout << "Specift Factor Sum Limit (Less or Equal to 2540160) -> " << endl;
	//cin >> factorSumLimit;

	int finalResult = 0;

	for (int currentNum = 10; currentNum < factorSumLimit; currentNum++) {
		int currentSum = 0;

		// Splits current number into it's digits
		int splitDigit = currentNum;

		while (splitDigit > 0) {
			// Add the factorial number of the right most digit, add to current sum, then remove from splitter

			currentSum += constFactorials[splitDigit % 10];
			splitDigit /= 10;
		}

		// If Sum of all numbers is equal to the number
		if (currentSum == currentNum) {
			finalResult += currentNum;
		}
	}

	cout << "Result -> " << finalResult << endl;
}

#pragma endregion Void Methods Available to Access

#pragma region Euler14
// The Maximum Number to cache data for (Only Effects Performance)
const int MaxNumber = 5000000 + 2;

// Default Value for non-computed chain sets
const int DefaultValue = -1;

// Stored Cached Chain Set, to reduce performance overhead
vector<short> cachedData(MaxNumber, DefaultValue);

// DetermineSteps: Retrieve the steps of the current Collatz Sequence, via recursion and cached data
short DetermineSteps(unsigned long long dataValue) {
    // If Value is 1, Recursion is finished and return 1
    if (dataValue == 1) {
        return 1;
    }

    // Otherwise, try to use cached result, if present, and not unknown/default
    if (dataValue < cachedData.size() && cachedData[dataValue] != DefaultValue) {
        return cachedData[dataValue];
    }

    // If both conditions fail, continue recursion
    long long nextValue = (dataValue % 2 == 0) ? (dataValue / 2) : (3 * dataValue + 1);
    auto newResult = 1 + DetermineSteps(nextValue);
    if (dataValue < cachedData.size()) {
        cachedData[dataValue] = newResult;
    }
    
    return newResult;
}

// LongestCollatzSequence: Project Euler Problem #14, determining which starting number produces the longest chain
void LongestCollatzSequence() {
    // Warn the User of Performance Overhead
    PrintLine("Method Warning: This Method, as defined in Euler, has significant performance overhead. Caution Advised!");
    
    // Format: smallestNumber, totalChainLength
    map<int, int> longestChainData;
    
    // Initial Storage of the Highest Analyzed Value
    int maxTested = 1;
    longestChainData[maxTested] = 1;
    
    // Iterate over Tests for the chain
    int maximumNumber = 0;
    maximumNumber = GetNumericAnswer("Input Maximum Number to check towards -> ", maximumNumber);
    
    while (maxTested <= maximumNumber) {
        // Retrieve the Chain Length
        auto chainSegmentLength = DetermineSteps(maxTested);
        
        // Check if as long as (or greater then) prior values, if any
        if (chainSegmentLength >= longestChainData.rbegin()->second) {
            longestChainData[maxTested] = chainSegmentLength;
        }
        
        // Move to the Next Test Value
        maxTested++;
    }
    
    // After Iteration, Find the Longest Chain bigger than the maximumNumber
    auto finalValue = longestChainData.upper_bound(maximumNumber);
    // Finalize Value, as the real answer is one step backwards
    finalValue--;
    
    // Print Output
    PrintLine("Result -> " + to_string(finalValue->first));
}
#pragma endregion Data to use with Collatz Sequence

#pragma region Euler20
// Store Factored Digits in a Reserved array, for further usage and to reduce performance overhead
typedef vector<int> DigitStorage;

// GetFactorialData: Returns and Assigns Data from Factoring Digits
DigitStorage GetFactorialData(int maximumFactor) {
	// Initialize and Reserve Original Data to a 1000 Number Limit (2568 Digits)
	DigitStorage finalData = { 1 };
	finalData.reserve(2568);

	// Iterate through Factoring Numbers
	for (int currentFactor = 2; currentFactor <= maximumFactor; currentFactor++) {
		// Multiple the stored digits with the currently iterating factor
		// Also Ensure an Overflow doesn't occue via carryover
		int carryByCount = 0;
		for (auto& currentDigit : finalData) {
			currentDigit = currentDigit * currentFactor + carryByCount;

			// Is Overflowed is Currently Factored Digit is at or above 10
			if (currentDigit >= 10) {
				carryByCount = currentDigit / 10;
				currentDigit %= 10;
			}
			else {
				// Reset Value if current digit is not overloaded
				carryByCount = 0;
			}
		}

		// Add any Remaining Carry Count
		while (carryByCount != 0) {
			finalData.push_back(carryByCount % 10);
			carryByCount /= 10;
		}
	}

	return finalData;
}

// GetFactorialDigitSum: Return the Sum of the Digits in "number!" (Multiply all digits in number, then add said digits again)
void GetFactorialDigitSum() {
	int evaluationNumber = 0;
	evaluationNumber = GetNumericAnswer("Enter Number to Get Factorial Digit Sum for (From 1 - 1000) -> ", evaluationNumber);

	// Gather the Sum of all Digits from the Factorial of the Input, then Output
	int factorialDigitSum = 0;
	for (auto factoredData : GetFactorialData(evaluationNumber)) {
		factorialDigitSum += factoredData;
	}

	PrintLine("The Factorial Digit Sum for " + to_string(evaluationNumber) + "! -> " + to_string(factorialDigitSum));
}
#pragma endregion Data to use with Factorial Digit Sum

// FinalExam10: Determining the smallest positive number evenly divisible by all of the numbers from 1 to 20
void FinalExam10() {
	bool foundNumber = false;
	int currentNumber = 2520; // 2520 = Smallest Number Divisible by all numbers 1 - 20

	// Continue Iterating by intervals of 2520 until Found Number
	while (!foundNumber) {
		currentNumber += 2520;
		bool isDivisible = true;

		// Divider Numbers 11 - 20 would be the same as 1 - 10, and are thus discounted from this loop to save time
		for (int currentDivider = 11; currentDivider <= 20; currentDivider++) {
			if (currentNumber % currentDivider != 0) {
				isDivisible = false;
				break;
			}
		}

		if (isDivisible) {
			foundNumber = true;
		}
	}

	PrintLine("Result -> " + to_string(currentNumber));
}

// FinalExam11: Finding abc for a Pythagoream Triplet for target sum of 1000
void FinalExam11() {
	int currentA = 0, targetResult = 1000;

	for (currentA = 1; currentA <= targetResult / 3; currentA++) {
		int currentB = 0;

		for (currentB = currentA + 1; currentB <= targetResult; currentB++) {
			// Since the sum of this Problem is meant to be 100, we can subtract both A and B values to get C
			int currentC = targetResult - currentA - currentB;

			// If A squared plus B squared == C squared
			// (pow isn't used here due to warning I'd have to ignore with Int->Double Casting)
			if (currentA * currentA + currentB * currentB == currentC * currentC) {
				PrintLine("Found Matching Values for Sum of 1000 as follows:");
				PrintLine("A = " + to_string(currentA));
				PrintLine("B = " + to_string(currentB));
				PrintLine("C = " + to_string(currentC));
				PrintLine("Product Result: " + to_string((double) (currentA * currentB * currentC)));
			}
		}
	}
}

// FinalExam12: Retreiving the Number of Solutions maximized for p <= 1000
void FinalExam12() {
	int maximizedNumber = 0, maxPValue = 0, targetSum = 1000;

	// Iterate Over Possible P Values towards less then or equal to the target maximum sum
	for (int currentPValue = 2; currentPValue <= targetSum; currentPValue += 2) {
		int currentC = 0;

		// Retrieve the Triplet Data Set values, to determine if the values fit for the specified p value
		for (int currentA = 2; currentA < currentPValue / 3; currentA++) {
			if (currentPValue * (currentPValue - 2 * currentA) % (2 * (currentPValue - currentA)) == 0) {
				currentC++;
			}
		}

		// If this is the new Highest Maximized Solution, update it ad the maximum P Value
		if (currentC > maximizedNumber) {
			maximizedNumber = currentC;
			maxPValue = currentPValue;
		}
	}

	// Output the Latest Resulting P Value
	PrintLine("Result -> " + to_string(maxPValue));
}

// MAIN LAUNCH FUNCTION, DO NOT DELETE!
int main()
{
	// Initialize Methods and Exit functions
	// Note: Reflection is Non-Existent in C/C++ as it is in Java, so this all needs to be done manually
	voidFunctionMap["Exit"] = AwaitInput;
	voidFunctionMap["Help"] = Help;

	voidFunctionMap["IntToCharacter"] = IntToCharacter;
	voidFunctionMap["ShapeAP"] = ShapeAP;
	voidFunctionMap["NumberExtender"] = NumberExtender;
	voidFunctionMap["StudentData"] = StudentData;
	voidFunctionMap["DigitAdder"] = DigitAdder;
	voidFunctionMap["ConeData"] = ConeData;
	voidFunctionMap["ConditionalTest"] = ConditionalTest;
	voidFunctionMap["ConditionalTest2"] = ConditionalTest2;
	voidFunctionMap["LoopTests"] = LoopTests;
	voidFunctionMap["LoopTests2"] = LoopTests2;
	voidFunctionMap["ArrayTesting"] = ArrayTesting;
	voidFunctionMap["FileInputTesting"] = FileInputTesting;
	voidFunctionMap["FileOutputTesting"] = FileOutputTesting;
	voidFunctionMap["ProfileDatabase"] = ProfileDatabase;
	voidFunctionMap["BMIFileTesting"] = BMIFileTesting;
	voidFunctionMap["SwitchTest"] = SwitchTest;
	voidFunctionMap["MonthlyWage"] = MonthlyWage;
	voidFunctionMap["Grade"] = Grade;
	voidFunctionMap["DayOfBirthWeek"] = DayOfBirthWeek;
	voidFunctionMap["PrintApple"] = PrintApple;
	voidFunctionMap["PerfectSquare_4Digit"] = PerfectSquare_4Digit;
	voidFunctionMap["StairCreator"] = StairCreator;
	voidFunctionMap["DetermineSeries"] = DetermineSeries;
	voidFunctionMap["DetermineSeriesSum"] = DetermineSeriesSum;
	voidFunctionMap["DetermineCompatibility"] = DetermineCompatibility;
	voidFunctionMap["CalculateMatchingPentagonals"] = CalculateMatchingPentagonals;
	voidFunctionMap["CountOfSundays"] = CountOfSundays;
	voidFunctionMap["LongestCollatzSequence"] = LongestCollatzSequence;
	voidFunctionMap["GetFactorialDigitSum"] = GetFactorialDigitSum;
	voidFunctionMap["FactorialSums"] = FactorialSums;

	voidFunctionMap["SuddenQuiz01"] = SuddenQuiz01;
	voidFunctionMap["SuddenQuiz02"] = SuddenQuiz02;
	voidFunctionMap["SuddenQuiz03"] = SuddenQuiz03;
	voidFunctionMap["SuddenQuiz04"] = SuddenQuiz04;
	voidFunctionMap["SuddenQuiz05"] = SuddenQuiz05;
	voidFunctionMap["SuddenQuiz06"] = SuddenQuiz06;
	voidFunctionMap["SuddenQuiz08"] = SuddenQuiz08;
	voidFunctionMap["Exam01"] = Exam01;
	voidFunctionMap["FinalExam10"] = FinalExam10;
	voidFunctionMap["FinalExam11"] = FinalExam11;
	voidFunctionMap["FinalExam12"] = FinalExam12;

	// Print Initial Text including Title and Version
	PrintLine(AppTitle + " - v" + version);
	PrintLine("Ported from Java 8 Project created in 2018-2019");
	PrintLine("Made by Chris Stack, 2019 - 2020");

	// Enter Loop for Inputting Data
	RequestInput();

	// Return Exit code at the End
	return EXIT_SUCCESS;
}
import inspect
import math

## Constant Variables
PI = 3.1415927
version = "1.0.5"
AppTitle = "Python Helper Bot"

methodFilter = ["introdialog", "requestinput", "countdigits", "getpentagonal", "getexpressionnumber"]
currentModule = __import__(__name__)

#region Utility Methods

## IntroDialog: Display Intro Text Data
def IntroDialog():
    print(AppTitle + " - v" + version)
    print("Ported from Java 8 Project created in 2018-2019")
    print("Made by Chris Stack, 2019 - 2020", end = "")

## Help: Output All Valid Methods in a Help Dialog
def Help():
    print(AppTitle + " - Valid Methods:")
    for i in dir(currentModule):
        if i.lower() not in methodFilter and inspect.isfunction(getattr(currentModule, i)):
            print("- " + i)

## CountDigits: Get integer of the digit result (Ex: CountDigits(112) = 1 + 1 + 2 = 4)
def CountDigits(inputNumber = 0, summarize = False):
    sum = 0
    while inputNumber != 0:
        sum += inputNumber % 10
        inputNumber = inputNumber // 10

    if summarize and sum >= 10:
        return CountDigits(sum, summarize)
    else:
        return sum

## GetPentagonal: Determine whether an Inputted Real Number or Index is a Pentagonal, and Return the Pentagonal
def GetPentagonal(x, isIndex = False):
    n = x

    if not isIndex:
        # If not an Index Number, IE a Sum or Real Number, SQRT it as assumed then use General Equation
        n = (1 + math.sqrt(24 * x + 1)) / 6

    pentagonalNumber = n * (3 * n - 1) / 2
    return pentagonalNumber == x, pentagonalNumber

## Exit: Dummy Blank Method to trigger exiting off of RequestInput
def Exit():
    print("Thanks for Using my HelperBot, Bye!")

## RequestInput: Port from Java (AskQuestion) to call a method name from a map of functions available
def RequestInput():
    foundMethod = False
    requestedMethod = input("\nPlease Input a Method Name (Type Help for help, or Exit to quit): ")
    
    if requestedMethod:
        for i in dir(currentModule):
            if not foundMethod and requestedMethod.lower() == i.lower() and requestedMethod.lower() not in methodFilter and inspect.isfunction(getattr(currentModule, i)):
                print("Launching Method: " + i + "...\n")
                foundMethod = True
                result = getattr(currentModule, i)()

        if not foundMethod:
            print("\nError: Method Name (" + requestedMethod + ") was not found! \nPlease Contact the author if this is a mistake... \n")
    else:
        print("Error: Please Input a Valid and Non-Empty Method Name...")

    if requestedMethod.lower() != "exit":
        RequestInput()

#endregion Utilities used in Methods and Primary Methods

#region Methods

## DigitAdder: Digits outputted as all digits added together
def DigitAdder():
    convertedString = input("Enter a Number: ")
    digitString = ""
    inputNumber = int(convertedString)
    sum = 0

    i = 0
    while i < len(convertedString):
        digitString = digitString + convertedString[i]

        if i != len(convertedString) - 1:
            digitString = digitString + ", "
        i += 1

    print("Digits (" + str(len(convertedString)) + "): [" + digitString + "]")
    print("Sum of all Digits: " + str(CountDigits(inputNumber)))

## StudentData: Getting Student Info such as Name, Favorite Class, and Student ID
def StudentData():
    userName = input("What's your name? ")
    print("Hello " + userName + ", It's very nice to meet you! :D")

    className = input("What is your favorite class, " + userName + "? ")
    print("That's great, I like " + className + " as well!")

    studentID = int(input("What is your Student ID? "))
    print("Thank You, " + userName + "! Your Student ID is " + str(studentID) + ".")

## NumberExtender: Extend a Number via operations from the initial entered number
def NumberExtender():
    operationID = input("Enter Operation (Empty/Invalid = Addition): ") or "+"

    # Convert OperationID to lowercase
    operationID = operationID.lower()

    enteredNumber = float(input("Input a Number: ") or 0)
    extendByNumber = float(input("Input a Number to adjust by (Negative to Reduce, Or Power if entered 'Pow' or '^' as selected operation): ") or 0)

    output = (enteredNumber - extendByNumber) if (operationID == "-" or operationID == "subtraction" or operationID == "subtract") else (enteredNumber * extendByNumber) if (operationID == "*" or operationID == "x" or operationID == "multiply" or operationID == "multiplication") else (enteredNumber / extendByNumber) if (operationID == "/" or operationID == "divide" or operationID == "division") else (enteredNumber % extendByNumber) if (operationID == "%" or operationID == "mod" or operationID == "modulus") else (math.pow(enteredNumber, extendByNumber)) if (operationID == "^" or operationID == "pow" or operationID == "power") else (math.sqrt(enteredNumber)) if (operationID == "sqrt") else (enteredNumber + extendByNumber)

    print("Operation in Use: " + operationID)
    print("Result: " + str(output))

## ShapeAP: Getting the Area and Perimeter of a Shape
def ShapeAP():
    length = float(input("Input the Length of your Shape: ") or 0)
    width = float(input("Input the Width of your Shape: ") or 0)

    area = length * width
    perimeter = (length * 2) + (width * 2)

    print("Shape Area: " + str(area))
    print("Shape Perimeter: " + str(perimeter))

## IntToCharacter: Converting an Integer to a ASCII Character
def IntToCharacter():
    initialChar = int(input("Input Number for Conversion to ASCII Character: ") or 0)
    convertedChar = chr(initialChar)
    print("Result: " + convertedChar)

## SuddenQuiz01: Sudden Quiz taken on 09/12/2019
def SuddenQuiz01():
    weight = float(input("Enter your weight in pounds: ") or 0)
    height_Feet = float(input("Enter your height in feet: ") or 0)
    height_Inches = float(input("Enter your height's remaining inches: ") or 0)

    # Height = Total Height in Inches
    height = (12 * height_Feet) + height_Inches

    BMI = (703 * weight) / (math.pow(height, 2))

    print("Your BMI is " + str(BMI) + ", your weight is " + str(weight) + ", and your height is " + str(height_Feet) + " feet and " + str(height_Inches) + " inches.")

## SuddenQuiz02: Sudden Quiz taken on 10/1/2019
def SuddenQuiz02():
    firstSum = 0
    secondSum = 0

    i = 1
    while i != 101:
        secondSum += i
        firstSum += math.pow(i, 2)
        i += 1

    secondSum = math.pow(secondSum, 2)

    print("The difference between the square of the sum (" + str(secondSum) + ")\nand the sum of the squares of the first one hundred natural numbers (" + str(firstSum) + ") is " + str(secondSum - firstSum) + ".")

## SuddenQuiz03: Sudden Quiz taken on 10/15/2019
def SuddenQuiz03():
    width = int(input("Enter Size of the Dynamic Z -> ") or 6)

    lineRow = 0
    while lineRow <= width:
        if (lineRow == 0 or lineRow == width):
            lineSpace = width
            while lineSpace > 0:
                print("O ", end ="")
                lineSpace -= 1
        else:
            positionToPlace = width - lineRow
            if positionToPlace != 1:
                while positionToPlace > 1:
                    print("  ", end ="")
                    positionToPlace -= 1
                print("O ", end ="")

        if (width - lineRow) != 1:
            print()

        lineRow += 1

## SuddenQuiz04: Sudden Quiz taken on 10/24/2019 (PrintWords: Print Word Count and occurance of characters for Sentence Inputted)
def SuddenQuiz04():
    # 96 Subtracted from ASCII if Lowercase; 64 if Uppercase

    wordCount = 0
    totalCharacterIndex = 0
    tempVariable = 0

    # Tuple<char, int>
    count = [0] * 999999

    # Tuple<int, int>
    charIntIndex = [0] * 999999

    sentenceInput = (input("Input a Sentence -> ")).lower()

    i = 0
    while i < len(sentenceInput):
        if sentenceInput[i] != '\0':
            if sentenceInput[i] == ' ':
                # Store tempVariable in charIntIndex and reset it
                charIntIndex[wordCount] = tempVariable
                tempVariable = 0

                # Add a word if space detected
                wordCount += 1
            else:
                # Add to Word Occurances
                count[ord(sentenceInput[i]) - 96] += 1

                # Add Character Value to temp Variable
                tempVariable += (ord(sentenceInput[i]) - 96)

        i += 1

    if tempVariable > 0:
        # Store final tempVariable in charIntIndex, then reset it
        charIntIndex[wordCount + 1] = tempVariable
        tempVariable = 0

    print("Number of Words: " + str(wordCount + 1))

    # Output the Character and Amount of Occurances
    countIndex = 0
    while countIndex < len(count):
        if count[countIndex] > 0:
            print("The character (" + chr(countIndex + 96).upper() + " or " + chr(countIndex + 96).lower() + ") occurs a total of " + str(count[countIndex]) + " times")

        countIndex += 1

    # Calculate Total Character Index by
    # Adding all Data from map then use countDigits
    # to get the sum of the digits at the end
    # Then output totalCharacterIndex
    for i in charIntIndex:
        totalCharacterIndex += i

    totalCharacterIndex = CountDigits(totalCharacterIndex)
    print("Total Character Index: " + str(totalCharacterIndex))

## GetExpressionNumber: Retrieve an Expression Number based on Character
def GetExpressionNumber(inputCharacter):
    switcher = {
        'a' or 'A': 1,
        'j' or 'J': 1,
        's' or 'S': 1,

        'b' or 'B': 2,
        'k' or 'K': 2,
        't' or 'T': 2,

        'c' or 'C': 3,
        'l' or 'L': 3,
        'u' or 'U': 3,

        'd' or 'D': 4,
        'm' or 'M': 4,
        'v' or 'V': 4,

        'e' or 'E': 5,
        'n' or 'N': 5,
        'w' or 'W': 5,

        'f' or 'F': 6,
        'o' or 'O': 6,
        'x' or 'X': 6,

        'g' or 'G': 7,
        'p' or 'P': 7,
        'y' or 'Y': 7,

        'h' or 'H': 8,
        'q' or 'Q': 8,
        'z' or 'Z': 8,

        'i' or 'I': 9,
        'r' or 'R': 9
        }
    return switcher.get(inputCharacter, 0)

## SuddenQuiz05: Sudden Quiz taken on 10/30/2019 (DetermineExpressionNumber: Retrieves an Expression Number from your Name)
def SuddenQuiz05():
    tempVariable = 0
    expressionNumber = 0

    nameInput = (input("Input your Name (Without Suffix/Prefix) -> ")).lower()

    i = 0
    while i < len(nameInput):
        if nameInput[i] != '\0':
            if nameInput[i] == ' ':
                # Space Detected, store tempVariable into final count and reset it

                # If not a Master Number (11, 22, 33, etc), reduce number with CountDigits
                if tempVariable % 11 != 0:
                    tempVariable = CountDigits(tempVariable)

                expressionNumber += tempVariable
                tempVariable = 0
            else:
                # For Each Letter, Add Expression Number of that Letter to Temp Variable
                tempVariable += GetExpressionNumber(nameInput[i])

        i += 1

    # Add Remainder of tempVariable
    if tempVariable % 11 != 0:
        tempVariable = CountDigits(tempVariable)

    expressionNumber += tempVariable
    tempVariable = 0

    # Add Up using same Master Number Calculation, but for final number
    # Also Sum Down CountDigits to One Digit Sum
    if expressionNumber % 11 != 0:
        expressionNumber = CountDigits(expressionNumber, True)

    print("Expression Number: " + str(expressionNumber))

## CalculateMatchingPentagonals: Determine for each value until maxIndex within distance if Sum and Difference are both Pentagonal's
def CalculateMatchingPentagonals():
    # TODO
    i = 0

## Exam01: Exam BB Questions done on 10/8/2019
def Exam01():
    # Q10 Data

    # 3 Rows on each side (2)
    lengthForSides = 3
    maxStars = 4
    sides = 2

    # Decrease stars as going towards the middle
    currentRow = lengthForSides * 2
    while currentRow >= 0:
        currentRowArea = maxStars
        while currentRowArea > 0:
            print("* ", end = "")
            currentRowArea -= 1

        if currentRow <= lengthForSides:
            if currentRow == lengthForSides + 1:
                # Middle Row
                print("*")

            maxStars += 1
        else:
            maxStars -= 1

        print()
        currentRow -= 1

## ConeData: Calculate the volume and surface area of a Cone using PI, radius, slant height, and the cone's height
def ConeData():
    inputHeight = float(input("Input Height of Cone: ") or 0)
    inputRadius = float(input("Input Radius of Cone: ") or 0)

    slantHeight = math.sqrt(math.pow(inputRadius, 2) + math.pow(inputHeight, 2))

    # PI * radius(radius + slantHeight)
    surfaceArea = PI * (inputRadius * (inputRadius + slantHeight))
    volume = PI * math.pow(inputRadius, 2) * (inputHeight / 3)

    print("The volume of a cone with a radius of " + str(inputRadius) + " and height of " + str(inputHeight) + " is (" + str(volume) + ")")
    print("The surface area of a cone with a radius of " + str(inputRadius) + " and height of " + str(inputHeight) + " is (" + str(surfaceArea) + ")")

## ConditionalTest: Boolean and If-Case Testing done on 09/17/2019 - Refactored on 09/26/2019
def ConditionalTest():
    maxTests = 3

    a = 0.0
    b = 0.0
    c = 0.0
    s = 0
    output = 0

    resultOutput = "Invalid Data, try again..."

    selection = int(input("Which Test do you wish to do? ") or 0)

    if selection == 0:
        a = float(input("Enter a Number for A: ") or 0)
        b = float(input("Enter a Number for B: ") or 0)
        c = float(input("Enter a Number for C: ") or 0)
        s = float(input("Enter an Integer for S: ") or 0)

        if s == 1:
            output = a + b + c
        elif s == 2:
            output = (b + c) / a
        elif s == 3:
            output = a - b
        elif s == 4:
            output = a * b * c
        elif s == 5:
            output = a / b
        elif s == 6:
            output = a % b
        elif s == 7:
            # NULL DATA
            output = 0
        elif s == 8:
            output = (a + b) / 10
        elif s == 9:
            output = (a / b) * 100

        if output != 0:
            resultOutput = str(output)
    elif selection == 1:
        a = int(input("Enter an Integer for A: ") or 0)

        if a != 0:
            resultOutput = (("Even") if (a % 2 == 0) else ("Odd")) + ((" Positive") if (a > 0) else (" Negative"))
        else:
            resultOutput = "Zero"

    print("Output: " + resultOutput)

## MonthlyWage: Calculating Regular, Overtime, and Triple Payouts for Pay Rate and Hours Worked
def MonthlyWage():
    workedHours = float(input("Input Hours worked in a week -> ") or 0)
    hourlyPayRate = float(input("Input Pay Rate per hour -> ") or 0)

    hoursOfOvertime = (((workedHours) if (workedHours <= 60) else 60) - 40) if (workedHours > 40) else 0
    hoursOfTriplePay = (workedHours - 60) if (workedHours > 60) else 0
    normalHours = workedHours - hoursOfOvertime - hoursOfTriplePay

    overtimePay = (1.75 * hourlyPayRate) * hoursOfOvertime
    triplePay = (3 * hourlyPayRate) * hoursOfTriplePay
    normalPay = hourlyPayRate * normalHours

    totalWeeklyPay = normalPay + overtimePay + triplePay
    totalMonthlyPay = totalWeeklyPay * 4

    print("Regular Pay: \t $" + str(normalPay))
    print("Overtime Pay: \t $" + str(overtimePay))
    print("Triple Pay: \t $" + str(triplePay))

    print("\nTotal Pay (Weekly): \t $" + str(totalWeeklyPay))
    print("Total Pay (Monthly): \t $" + str(totalMonthlyPay))

## ConditionalTest2: Secondary Testing of If-Else Statements done on 09/24/2019
def ConditionalTest2():
    evenNum = False

    a = int(input("Input a Number: ") or 0)
    evenNum = a % 2 == 0

    if a < 100:
        if a < 50:
            print("apple" if evenNum else "carrot")
        else:
            print("orange" if evenNum else "eggplant")
    else:
        if a > 150:
            print("watermelon" if evenNum else "potato")
        else:
            print("papaya" if evenNum else "tomato")

## Grade: Outputs a Grade Letter based on Percentage
def Grade():
    initialNumber = float(input("Input the total points you received: ") or 0)
    totalNumber = float(input("Input the total grade points (Leave Blank for 100): ") or 100)

    a = initialNumber / totalNumber
    gradeLetter = ("A") if (a >= 90) else ("B") if (a >= 80) else ("C") if (a >= 70) else ("D") if (a >= 60) else ("F")

    print("Total: " + str(initialNumber) + "/" + str(totalNumber) + "(" + str(a) + " - " + gradeLetter + ")")
    print("Your GPA is " + str(a / 25))

## LoopTests: Loop Testing done on 09/26/2019 (Alternate Name: SequenceDesigner)
def LoopTests():
    optionalOutput = ""
    currentSum = 0.0

    optionalInput = input("Enable Static Sequence Mode (Printing a Seperated Sequence of numbers) -> ")
    staticSequence = optionalInput.title() == "Y" or optionalInput.title() == "True"

    startNumber = float(input("Enter a Number for the Start Number: ") or 0)
    maxNumber = float(input("Enter a Number for the Starting Number to Count towards: ") or 0)
    countBy = float(input("Enter a Number to select mode of counting above 1 (0 for Normal): ") or 0)
    powerMode = float(input("Enter a power mode, or press 0 for normal (1 for within, 2 for after): ") or 0)
    powerAmount = float(input("Enter a power amount, or presss 0 for None: ") or 0)

    startNumber = (maxNumber - 1) if (startNumber >= maxNumber) else (startNumber)

    currentNumber = startNumber
    while currentNumber <= maxNumber:
        currentSum += (math.pow(currentNumber, powerAmount)) if (powerMode == 1) else (currentNumber)
        if staticSequence:
            optionalOutput += str(currentNumber) + (("") if (currentNumber >= maxNumber) else (", "))
        else:
            print("Current Sum at " + str(currentNumber) + ": " + str(currentSum))

        currentNumber += (countBy) if (countBy > 0) else (1)

    if powerMode == 2:
        currentSum = math.pow(currentSum, powerAmount)

    print("Result: Sucessfully Counted/Added from " + str(startNumber) + " to " + str(maxNumber))

    if optionalOutput:
        print("Output: { " + optionalOutput + " }")

## LoopTests2: Secondary Loop Testing done on 10/3/2019
def LoopTests2():
    # Q4/Q5
    sidedLength = int(input("Set the Length for the Maximum sided Length: ") or 4)

    # Q2
    i = 0
    while i < sidedLength:
        spaces = 0

        # Q4
        excess = i

        while spaces < sidedLength - (i + 1):
            print(" ", end = "")
            spaces += 1

        while spaces < sidedLength:
            print("*", end = "")
            spaces += 1

        # Q4
        while excess != 0:
            print("*", end = "")
            excess -= 1

        print()
        i += 1

    # Q4 Middle
    maxLength = sidedLength * 2
    while maxLength != 0:
        print((">" if (maxLength % 2 == 0) else "<"), end = "")
        maxLength -= 1
    print()

    # Q3
    i = 0
    while i < sidedLength:
        spaces = i
        remaining = sidedLength - i

        # Q4
        excess = remaining - 1

        while spaces != 0:
            print(" ", end = "")
            spaces -= 1

        while remaining != 0:
            print("^", end = "")
            remaining -= 1

        # Q4
        while excess != 0:
            print("^", end = "")
            excess -= 1

        print()
        i += 1

## DayOfBirthWeek: Calculates and Outputs the Day of the Week of Birth
def DayOfBirthWeek():
    total = 0
    outputMonth = ""

    inputMonth = int(input("Input Month (Format: XX or X) -> ") or 0)
    inputDay = int(input("Input Day (Format: XX or X) -> ") or 0)
    inputFullYear = int(input("Input Full Year (Format: XXXX) -> ") or 0)

    inputYear_FirstTwo = (inputFullYear / 100)
    inputYear_LastTwo = (inputFullYear % 100)

    # Determine if Inputted Year is a leap year
    # A. Divisible by 4
    # B. NOT divisible by 100 OR is also divisible by 400
    isLeapYear = (inputFullYear % 4 == 0) and (inputFullYear % 100 != 0 or inputFullYear % 400 == 0)

    # Getting the Month Number and Adding to Total
    # (Last 2 Digits of Birth Year / 4) + 2 Digit Birth Year + Two Digit Birth Day
    total += (inputYear_LastTwo / 4) + inputYear_LastTwo + inputDay

    # Set OutputMonth and Add to total depending on Inputted Month
    if inputMonth == 1:
        total += 1
        outputMonth = "January"
    elif inputMonth == 2:
        total += 4
        outputMonth = "February"
    elif inputMonth == 3:
        total += 4
        outputMonth = "March"
    elif inputMonth == 4:
        # total += 0
        outputMonth = "April"
    elif inputMonth == 5:
        total += 2
        outputMonth = "May"
    elif inputMonth == 6:
        total += 5
        outputMonth = "June"
    elif inputMonth == 7:
        # total += 0
        outputMonth = "July"
    elif inputMonth == 8:
        total += 3
        outputMonth = "August"
    elif inputMonth == 9:
        total += 6
        outputMonth = "September"
    elif inputMonth == 10:
        total += 1
        outputMonth = "October"
    elif inputMonth == 11:
        total += 4
        outputMonth = "November"
    elif inputMonth == 12:
        total += 6
        outputMonth = "December"
    else:
        # Default: Invalid Value
        outputMonth = ""

    # IF Leap year and Month is January or February subtract 1 from total
    if isLeapYear and (inputMonth == 1 or inputMonth == 2):
        total -= 1

    a = int(inputYear_FirstTwo % 4)

    # Default: If a is an invalid value
    total += 6 if (a == 0) else 4 if (a == 1) else 2 if (a == 2) else 0

    # Warn against determining the day of birth for years earlier then 1900
    if inputYear_FirstTwo < 19:
        print("Warning: Advisory against using Day Determination for Years earlier then 1900")

    # Find, Calculate, and Output the Final Day
    b = int(total % 7)

    dayOfWeek = {
        1: "Sunday",
        2: "Monday",
        3: "Tuesday",
        4: "Wednesday",
        5: "Thursday",
        6: "Friday",
        0: "Saturday"
        }

    # Default: Invalid Value
    outputDayOfWeek = dayOfWeek.get(b, "")

    print("A person born on " + outputMonth + " " + str(inputDay) + ", " + str(inputFullYear) + " was born on a " + outputDayOfWeek + ", and " + ("is" if isLeapYear else "is not") + " a leap year.")

## PerfectSquare_4Digit: Calculates whether an inputted 4-digit number is a perfect square
def PerfectSquare_4Digit():
    automaticModeInput = input("Use Automatic Detection? (Y/N) -> ")
    automaticMode = automaticModeInput.title() == "Y" or automaticModeInput.title() == "True"

    inputNumber = 0

    squareRooted = 0.0
    sumMultiplied = 0.0

    if automaticMode:
        inputNumber = 32
        while inputNumber <= 99:
            sumMultiplied = math.pow(inputNumber, 2)

            # If Sum of digitsAdded is the square root of inputNumber, thus having sumMultiplied equal the inputNumber
            # Then inputNumber is a 4-digit perfect square
            print("The result of " + str(inputNumber) + ": (" + str(sumMultiplied) + ") is a 4-digit perfect square!")

            inputNumber += 1
    else:
        inputNumber = int(input("Input a 4 Digit Number -> ") or 0)

        squareRooted = math.sqrt(inputNumber)
        sumMultiplied = math.pow(squareRooted, 2)

        # If Sum of digitsAdded is the square root of inputNumber, thus having sumMultiplied equal the inputNumber
        # Then inputNumber is a 4-digit perfect square
        print("The number " + str(inputNumber) + " " + ("is" if (sumMultiplied == inputNumber) else "is not") + " a 4-digit perfect square!")

## StairCreator: Creates a Set of Stairs with a specified Row Count and interchangable characters
def StairCreator():
    character1 = "$"
    character2 = "#"

    # Config Variables
    interchangeCharactersInput = ""
    interchangeCharacters = False

    customizeCharactersInput = ""
    customizeCharacters = False

    usingSecondCharacterInput = ""
    usingSecondCharacter = False

    spaceBetweenLinesInput = ""
    spaceBetweenLines = True

    spaceBetweenCharactersInput = ""
    spaceBetweenCharacters = False

    rows = int(input("Input Amount of Rows to Generate -> ") or 0)

    customizeOptionsInput = input("Do you wish to Customize Options (Y/N) -> ")
    customizeOptions = customizeOptionsInput.title() == "Y" or customizeOptionsInput.title() == "True"

    if customizeOptions:
        # Configuration Options
        interchangeCharactersInput = input("Do you wish to Interchange Characters between Rows (Y/N) (Default: N) -> ")
        interchangeCharacters = interchangeCharactersInput.title() == "Y" or interchangeCharactersInput.title() == "True"

        spaceBetweenLinesInput = input("Do you wish to have an Empty Line (A Space) between each line (Y/N) (Default: Y) -> ") or "Y"
        spaceBetweenLines = spaceBetweenLinesInput.title() == "Y" or spaceBetweenLinesInput.title() == "True"

        spaceBetweenCharactersInput = input("Do you wish to have a space between each character in a line (Y/N) (Default: N) -> ")
        spaceBetweenCharacters = spaceBetweenCharactersInput.title() == "Y" or spaceBetweenCharactersInput.title() == "True"

        # Customizing Characters
        if interchangeCharacters:
            customizeCharactersInput = input("Do you wish to Customize Characters to Generate (Y/N) -> ")
            customizeCharacters = customizeCharactersInput.title() == "Y" or customizeCharactersInput.title() == "True"

            if customizeCharacters:
                character1 = input("Input First Character (Default: $) -> ") or "$"
                character2 = input("Input Second Character (Default: #) -> ") or "#"

    # LOOP GENERATION
    currentRow = 1
    while currentRow <= rows:
        currentPosition = 1
        while currentPosition <= currentRow:
            print((character2 if usingSecondCharacter else character1) + (" " if spaceBetweenCharacters else ""), end = "")
            currentPosition += 1

        if spaceBetweenLines:
            print()

        if interchangeCharacters:
            usingSecondCharacter = not usingSecondCharacter

        currentRow += 1

    print("\n Stair Generation Completed Successfully!")
#endregion


## Main Events - DO NOT REMOVE
IntroDialog()
print()
RequestInput()
